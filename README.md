
nonmateria-opcodes 
================

csound opcodes based on the csound [Plugin Opcode SDK](https://github.com/csound/opcode_sdk)

included opcodes:

- `sinefold` sine shaped wavefolder
- `trifold` triangle shaped wavefolder 

instruction for debian 11:

```
sudo apt-get install libcsnd-dev
git clone https://codeberg.org/nonmateria/nonmateria_opcodes
cd nonmateria_opcodes
cmake .
make 
sudo cp *.so /usr/lib/x86_64-linux-gnu/csound/plugins64-6.0/
```

License
--------------
Nicola Pisanti LGPL License 2021
