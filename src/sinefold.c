
#include <csdl.h>

#include <math.h>

#define WF_TWO_PI 6.2831853071795864769252867665590f

typedef struct {
	OPDS h;
	MYFLT *out, *in, *control, *feedback;
	MYFLT z1;
} SINEFOLD;


int init(CSOUND *csound, SINEFOLD *p) {
 	p->z1 = 0.0f;
	return OK;
}


int sinefold_scalar(CSOUND *csound, SINEFOLD *p) {
	MYFLT ctrl = *(p->control);
	MYFLT fb = *(p->feedback);
	
	MYFLT xn = *p->in;
	MYFLT yn = sinf(WF_TWO_PI * xn * ctrl);

	// saturated feedback 
	yn += tanhf(p->z1 * fb);
	p->z1 = yn;
	*p->out = yn; 
	return OK;
}

int sinefold_vector(CSOUND *csound, SINEFOLD *p) {
	MYFLT *out = p->out;
	MYFLT *in = p->in;
	MYFLT ctrl = *(p->control);
	MYFLT fb = *(p->feedback);
	
	uint32_t    offset = p->h.insdshead->ksmps_offset;
	uint32_t    early  = p->h.insdshead->ksmps_no_end;
	uint32_t    n, nsmps = CS_KSMPS;

	if (UNLIKELY(offset)) memset(out, '\0', offset*sizeof(MYFLT));
	if (UNLIKELY(early)) {
		nsmps -= early;
		memset(&out[nsmps], '\0', early*sizeof(MYFLT));
	}

	// sine folding
	for(n = offset; n < nsmps; n++){
		float xn = in[n];
		float yn = sinf(WF_TWO_PI * xn * ctrl);

		// saturated feedback 
		yn += tanhf(p->z1 * fb);
		p->z1 = yn;
		out[n] = yn; 
	}
   
  return OK;
}

static OENTRY localops[] =
  {
   { "sinefold.akk", sizeof(SINEFOLD), 0, 2, "a", "akk",
     NULL, (SUBR) sinefold_vector },   
   { "sinefold.akk", sizeof(SINEFOLD), 0, 2, "a", "aii",
     NULL, (SUBR) sinefold_vector },
   { "sinefold.kkk", sizeof(SINEFOLD), 0, 2, "k", "kkk",
     NULL, (SUBR) sinefold_scalar },
   { "sinefold.iii", sizeof(SINEFOLD), 0, 1, "i", "iii",
        (SUBR) sinefold_scalar, NULL }
  };

LINKAGE
