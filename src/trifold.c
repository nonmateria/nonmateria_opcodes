
#include <csdl.h>

#include <math.h>

#define WF_TWO_PI 6.2831853071795864769252867665590f

typedef struct {
	OPDS h;
	MYFLT *out, *in, *control, *feedback;
	MYFLT z1;
} TRIFOLD;


int init(CSOUND *csound, TRIFOLD *p) {
 	p->z1 = 0.0f;
	return OK;
}


int trifold_scalar(CSOUND *csound, TRIFOLD *p) {
	MYFLT ctrl = *(p->control);
	MYFLT fb = *(p->feedback);
	
	MYFLT xn = *p->in;
	MYFLT yn = sinf(WF_TWO_PI * xn * ctrl);

	// saturated feedback 
	yn += tanhf(p->z1 * fb);
	p->z1 = yn;
	*p->out = yn; 
	return OK;
}

int trifold_vector(CSOUND *csound, TRIFOLD *p) {
	MYFLT *out = p->out;
	MYFLT *in = p->in;
	MYFLT ctrl = *(p->control);
	MYFLT fb = *(p->feedback);
	
	uint32_t    offset = p->h.insdshead->ksmps_offset;
	uint32_t    early  = p->h.insdshead->ksmps_no_end;
	uint32_t    n, nsmps = CS_KSMPS;

	if (UNLIKELY(offset)) memset(out, '\0', offset*sizeof(MYFLT));
	if (UNLIKELY(early)) {
		nsmps -= early;
		memset(&out[nsmps], '\0', early*sizeof(MYFLT));
	}

	// triangle folding
	float P = 1.0f / ctrl;
	for(n = offset; n < nsmps; n++){
		float xn = in[n] + (P * 0.25f);
		float x_slash_p = xn / P;
		float yn = 4.0f * fabsf(x_slash_p - floorf(x_slash_p + 0.5f)) - 1.0f;

		// saturated feedback 
		yn += tanhf(p->z1 * fb);
		p->z1 = yn;
		out[n] = yn; 
	}
   
  return OK;
}

static OENTRY localops[] =
  {
   { "trifold.akk", sizeof(TRIFOLD), 0, 2, "a", "akk",
     NULL, (SUBR) trifold_vector },   
   { "trifold.akk", sizeof(TRIFOLD), 0, 2, "a", "aii",
     NULL, (SUBR) trifold_vector },
   { "trifold.kkk", sizeof(TRIFOLD), 0, 2, "k", "kkk",
     NULL, (SUBR) trifold_scalar },
   { "trifold.iii", sizeof(TRIFOLD), 0, 1, "i", "iii",
        (SUBR) trifold_scalar, NULL }
  };

LINKAGE
